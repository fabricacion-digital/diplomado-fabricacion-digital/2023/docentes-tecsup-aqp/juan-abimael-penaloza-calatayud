# 1. Principles and practices

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research

"Many times we ask ourselves how we can learn a topic, without knowing how to start or finish, however, gamification is a way of learning any topic like playing, that is why I propose an adaptive alternative to any specific topic, based on questions and answers, with a geometric board, in high relief.."
![](../images/gm.png)


## Description and prototype of the final work

- The proposal for final work is a board game, with QR code validation.
It consists of defining a specific theme, for example: "Elementary Physics", each participant chooses an avatar, both physical and virtual, then on the board, they are positioned at the starting point, but each participant must place a sensor on the finger, where each avatar moves according to the movement of the finger of each participant, then the questions begin by levels, there are 3 levels:
Beginner.
Intermediate.
Advanced.
Beginner questions are worth 3 points, intermediate questions are worth 6 points, and advanced questions are worth 9 points.
While the game is developing, you have to answer a riddle, a challenge, or a selfie which will be stored in the cloud.
The game ends when one of the 4 participants reaches the finish line first.
![](../images/tf.png)

## What will you do?

I want to use gamification to be able to learn any subject, since both questions and answers can be generated among new participants, and those who adapt to the methodology.
The idea is to use a movement sensor, to synchronize the avatar of each participant, with her finger, in such a way that the movement of the finger on the avatar (collectible by theme) can be seen.
In short, in a board game adaptable to multiple environments.
Use the three backticks to separate code.


## Who will use it?

All the general public will be able to use it, from 10 years of age onwards, be they children, students, teachers, administrators, or others.
