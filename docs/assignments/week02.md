# 2. Project management

This week I worked on defining my final project idea and started to getting used to the documentation process.

## How work with git

The first step to be able to edit the fab academic repository is to download git:

![](../images/Imagen1.jpg)


After downloading git.
We proceed to clone the repository, in this way:
"git clone"
![](../images/Imagen2.jpg)

But it is copied or cloned from the same repository:
![](../images/Imagen3.jpg)

After cloning the base repository, we are going to identify and validate the password, in the git lab platform.
![](../images/Imagen4.jpg)
![](../images/Imagen4.1.jpg)

Once the users and accesses are defined, the SSH Keys are generated and entered, in git lab
![](../images/Imagen5.jpg)
![](../images/Imagen6.jpg)

Now we proceed to configure the username and email
![](../images/Imagen7.jpg)


In order for the information to be published in git lab online, the user must be validated or authenticated with a debit or credit card.
And then we proceed to review the pages published on the web.
![](../images/Imagen8.jpg)

In order to publish any changes made in the sublime text, we do it from the git CMD, with these commands:
![](../images/Imagen9.jpg)

## How work with sublime text

At the same time that git CMD is downloaded, we download sublime text, so that we can more easily edit the content of the website
![](../images/Imagen1.1.jpg)

At the same time that git is used, we use sublime text to edit the content, which is going to be reflected on the web, as shown below:
![](../images/Imagen10.jpg)
![](../images/Imagen11.jpg)
![](../images/Imagen12.jpg)
