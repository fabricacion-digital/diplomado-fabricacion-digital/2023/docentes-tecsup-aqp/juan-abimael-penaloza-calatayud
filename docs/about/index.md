# About me

<center>![](../images/foto.png)</center>

Greetings
I am the teacher Juan Peñaloza Calatayud.
With an Academic Degree in Industrial Engineering from the National University of San Agustín, College of Engineers of Arequipa, CIP: 219177, with Master's Studies in Business Administration, having developed functions in companies: Grupo Dusantos EIRL; Incalpaca; M&J Integral Consulting EIRL; District Municipality of Atico, among others.


## My youtube channel

A few years ago I created my YouTube channel, where I upload information on education, technology and entrepreneurship.
On my channel you can see many videos, so that you have a reference in your professional development.
I invite you to subscribe.

- [Juan Peñaloza](https://www.youtube.com/channel/UCyXmP5OuiaAJ00MaZy-LVKQ)


### Achievements achieved

In 2021, I obtained this recognition, from my workplace:
![](../images/logro.jpeg)


### Fulfilled projects
On June 23, 2023, I was able to lead the I Expo Tecsup - Category C: Entrepreneurship.
It was an enriching experience, where I was able to support 10 entrepreneurial groups to participate, as well as materialize their business idea into something tangible and functional.
![](../images/feria.jpeg)

I also participated in a Contest held by my workplace, with this initiative:
<center><iframe width="560" height="315" src="https://www.youtube.com/embed/Uxzvgc4PJzY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></center>