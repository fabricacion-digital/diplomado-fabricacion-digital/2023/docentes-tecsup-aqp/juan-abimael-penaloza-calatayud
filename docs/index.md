# HOME

## HELLO EVERYONE

![](./images/week01/5.png)

## FAB ACADEMY SITE

On this site you can find relevant information on digital manufacturing and other related aspects.

## ABOUT FAB LAB

The ease of access to technology has led to the arrival of a new industrial revolution that enables citizens to stop being mere users and become creators and builders of their own goods, relying on social trends based on collaborative culture. The emergence of Fablabs -Fabrication Laboratory- as environments that enhance these capabilities in users has been decisive in this technological democratization, and it continues to grow incessantly, spreading dramatically throughout the planet. The lack of theoretical references and the limited academic bibliography on the subject makes it necessary to design this tool based on contributions from experts in the FabLab phenomenon and, for this, its validation using the Delphi method was considered adequate, since it allows the transmission of the subjective information provided by the experts, respecting their peculiarities.
![](./images/week01/11.png)

## REFERENCE

- Lena-Acebo, F.J. (2018). Application of the Delphi method in the design of a quantitative investigation on the FABLAB phenomenon. EMPIRIA. Journal of Methodology of the Social Sciences, (40), 129-166.